<?php

use Illuminate\Database\Seeder;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;
        DB::table('teachers')->truncate();
        
        do {
            DB::table('teachers')->insert([
                'name' => $this->name(),
                'department' => $this->department()
            ]);

            $i++;
        } while ($i < 19);
    }

    private function name() {
        $names = collect([
            'Mohamad Ali Suhail',
            'Haneef Khan',
            'Adeel Ahmad',
            'Ibrahim Alshourbgi',
            'Intkhab',
            'Hussain Zangoti',
            'Barakash Kubus',
            'Mohamad Rafeeq',
        ]);

        return $names->random();
    }
    
    private function department() {
        $departments = collect([
            'Computer Networks',
            'Computer Science',
            'Information Systems',
        ]);

        return $departments->random();
    }
}
