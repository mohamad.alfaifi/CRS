<?php

use Illuminate\Database\Seeder;

class TimingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('timings')->truncate();
        $i = 0;

        do {

            $period = $this->period();

            $timing = \App\Timing::create([
                'starts_at' => $period[0],
                'ends_at' => $period[1],
                'day' => collect([1, 2, 3, 4, 5])->random(),
            ]);

            $teacher = \App\Teacher::find(collect(range(1,19))->random());
            $course = \App\Course::find(collect(range(1,50))->random());

            $teacher->timings()->save($timing);
            $course->timings()->save($timing);

            $i++;
        } while ($i < 100);
    }

    private function period() {
        return collect([
            [ '08:00', '08:50' ],
            [ '09:00', '09:50' ],
            [ '10:00', '10:50' ],
            [ '11:00', '11:50' ],
            [ '12:00', '12:50' ],
            [ '13:00', '13:50' ],
            [ '14:00', '14:50' ],
            [ '15:00', '15:50' ],
            [ '16:00', '16:50' ],
            [ '17:00', '17:50' ],
        ])->random();
    }
}
