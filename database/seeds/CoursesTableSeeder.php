<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->truncate();
        $i = 0;

        do {
            DB::table('courses')->insert([
                'name' => $this->name(),
                'department' => $this->department(),
                'level' => collect(range(1,10))->random(),
                'code' => $this->code(),
            ]);
            $i++;
        } while ($i < 58);
    }

    private function name() {
        $names = collect([
            'Object Oriented Programming',
            'Signals & Systems',
            'Agorithms & Data Structures 1',
            'Differentiation & Integration',
            'Electronic Circuits 1',
            'Digital Logic',
            'Introduction to Information System',
            'Programming 2',
            'Intensive English Course 2',
            'Java Programming',
            'Internet Technology',
            'Operating System',
            'Introduction to Communication System'
        ]);


        return $names->random();
    }

    private function level() {
        $levels = collect([
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
        ]);

        return $levels->random();
    }

    private function department() {
        $departments = collect([
            'Computer Networks',
            'Computer Science',
            'Information Systems',
        ]);

        return $departments->random();
    }

    private function code() {
        $codes = collect([
            '441CNET-3',
            '495CNET-3',
            '447CNET-3',
            '448CNET-3',
            '**4CNET-3',
            '390CNET-3',
            '101CSC-3',
            '100MATH-3',
            '102SLM-2',
            '104ENGL-6',
            '201PHYS-4',
            '112COMP-3',
            '111INFS-3',
            '241INFS-3',
            '452INFS-3',
            '213COMP-3',
            '213CNET-3',
            '102ARB-2',
        ]);

        return $codes->random();
    }
}
