<?php

Auth::routes();

Route::get('/', 'StudentController@index');
Route::prefix('/student')->group(function() {
    Route::get('/courses/list', 'StudentController@listAllCourses');


    Route::get('/courses/{id}/add', 'StudentController@addCourse')->where('id', '[0-9]+');
    Route::get('/courses/{id}/delete', 'StudentController@deleteCourse')->where('id', '[0-9]+');
});

Route::prefix('/admin')->group(function() {
    Route::get('/', 'AdminController@index');


    Route::get('/courses/new', 'AdminController@showNewForm');
    Route::post('/courses/new', 'AdminController@storeNewForm');


    Route::get('/courses/set', 'AdminController@showSetForm');
    Route::post('/courses/set', 'AdminController@storeSetForm');
});