<?php

namespace App\Http\Controllers;

use App\Timing;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $timings = \Auth::user()->timings()->get();
        return view('student.home', compact('timings', $timings));
    }

    public function listAllCourses()
    {
        $timings = \App\Timing::all()
            ->reject(function($item) {
                return $item->course->level <= \Auth::user()->level;
            })
            ->reject(function($item) {

                foreach (\Auth::user()->timings as $timing) {
                    if ($timing->id == $item->id) {
                        return true;
                    }
                }

                return false;

            })
            ->reject(function($item) {

                if ($item->course->department !== \Auth::user()->department) {
                    return true;
                }

                return false;

            })->all();

        return view('student.list-courses', compact('timings', $timings));
    }

    public function addCourse($id)
    {

        if ( count(\Auth::user()->timings) === 5 ) {
            session()->flash('threshold', 'Sorry Forgot to say that you can only add 5 courses.' );
            return redirect('/student/courses/list');
        }

        $timing = \App\Timing::find($id);

        if (count($timing->users) >= 2) {
            session()->flash('threshold', 'Class is full.' );
            return redirect('/student/courses/list');
        }

        $colision = null;

        foreach (\Auth::user()->timings as $userTiming) {

            if ($userTiming->starts_at == $timing->starts_at && $userTiming->day == $timing->day) {
                $colision = $userTiming;
            }

        }

        if (! $colision) {
            session()->flash('success', 'Success! no collisions.' );
            \Auth::user()->timings()->attach($id);
        } else {
            session()->flash('collision', 'on day no.' . $timing->day . ' course [' . $colision->course->code . '] starts at (' . $colision->starts_at . ') HOWEVER course [' . $timing->course->code . '] starts at (' . $timing->starts_at . ') as well' );
        }


        return redirect('/student/courses/list');
    }

    public function deleteCourse($id)
    {
        \Auth::user()->timings()->detach($id);

        return redirect('/');
    }
}
