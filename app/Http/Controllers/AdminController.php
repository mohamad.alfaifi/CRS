<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() {
        $courses = \App\Course::all();
        return view('admin.index', compact('courses', $courses));
    }

    public function showNewForm() {
        return view('admin.form-add-course');
    }

    public function storeNewForm(Request $request) {

        $results = \App\Course::create([
            'name' => $request->name,
            'code' => $request->code,
            'department' => $request->department,
            'level' => $request->level
        ]);

        if ($results) {
            session()->flash('status', 'Course Saved!');
        }

        return redirect('/admin/courses/new');
    }

    public function showSetForm() {
        $courses = \App\Course::all();
        $teachers = \App\Teacher::all();

        return view('admin.form-set-course-time',
            compact('courses', $courses),
            compact('teachers', $teachers));
    }

    public function storeSetForm(Request $request) {

        $timing =  \App\Timing::create([
            'day' => $request->day,
            'starts_at' => $request->starts_at,
            'ends_at' => $request->ends_at,
        ]);

        \App\Teacher::find($request->teacher)->timings()->save($timing);
        \App\Course::find($request->course)->timings()->save($timing);


        if ($timing) {
            session()->flash('status', 'all students can see it now!');
        }

        return redirect('/admin/courses/set');
    }
}
