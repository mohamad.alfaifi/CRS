<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timing extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'starts_at', 'ends_at', 'day', 'course_id', 'teacher_id'
    ];

    public function users() {
        return $this->belongsToMany(\App\User::class);
    }

    public function course() {
        return $this->hasOne(\App\Course::class, 'id', 'course_id');
    }

    public function teacher() {
        return $this->hasOne(\App\Teacher::class, 'id', 'teacher_id');
    }
}
