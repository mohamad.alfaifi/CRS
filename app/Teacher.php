<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'department',
    ];

    public function courses() {
        return $this->hasManyThrough(\App\Course::class, \App\Timing::class);
    }

    public function timings() {
        return $this->hasMany(\App\Timing::class);
    }
}
