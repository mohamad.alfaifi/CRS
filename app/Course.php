<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'department', 'level', 'code'
    ];

    public function timings() {
        return $this->hasMany(\App\Timing::class);
    }
}
