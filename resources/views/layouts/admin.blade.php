<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Jazan University -- ADMIN Portal</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top" style="background-color: #000;
border-color: #c2ced7; color: white;">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="#" style="color: white">
                    Jazan University
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">

            <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li class=" {!! Request::is('admin') ? 'active' : '' !!}"><a href="{{ url('/admin/') }}" {!! Request::is('admin') ? '' : 'style="color: white"' !!}>Filter Courses</a></li>
                    <li class=" {!! Request::is('admin/courses/new') ? 'active' : '' !!}"><a href="{{ url('/admin/courses/new') }}" {!! Request::is('admin/courses/new') ? '' : 'style="color: white"' !!}>Add Course</a></li>
                    <li class=" {!! Request::is('admin/courses/set') ? 'active' : '' !!}"><a href="{{ url('/admin/courses/set') }}" {!! Request::is('admin/courses/set') ? '' : 'style="color: white"' !!}>Timing</a></li>
                </ul>

            <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a style="color: white">ADMINISTRATOR</a></li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
</div>
<footer>
    <center>
        <h5>GRADUATION PROJECT BY Mohamad Alfaifi.</h5>
    </center>
</footer>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
