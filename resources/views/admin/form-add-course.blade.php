@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if (session('status'))
                    <div class="alert alert-success alert-dismissable">
                        <strong>Success!</strong>&nbsp;{{ session('status') }}
                    </div>
                @endif


                <form method="post" action="{{ url('/admin/courses/new') }}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="firendly-name">FRIENDLY NAME:</label>
                        <input type="text" class="form-control" name="name" id="firendly-name">
                    </div>
                    <div class="form-group">
                        <label for="code">CODE NAME:</label>
                        <input type="text" name="code" class="form-control" id="code">
                    </div>
                    <div class="form-group">
                        <label for="dept">DEPARTMENT:</label>
                        <select class="form-control" name="department" id="dept">
                            <option>Computer Science</option>
                            <option>Information Systems</option>
                            <option>Computer Networks</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="lvl">LEVEL:</label>
                        <select class="form-control" name="level" id="lvl">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                        </select>
                    </div>


                    <button type="submit" class="btn btn-default">SAVE IT!</button>
                </form>


            </div>
        </div>
    </div>
@endsection
