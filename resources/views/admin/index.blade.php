@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-4">
                filters goes in here
            </div>


            <div class="col-md-8">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <table class="table table-bordered table-hover">
                    <thead class="bg-warning">
                    <tr>
                        <th>NAME</th>
                        <th>DEPARTMENT</th>
                        <th>LEVEL</th>
                        <th>CODE</th>
                    </tr>
                    </thead>
                    <tbody style="background-color: white;">

                    @foreach($courses as $course)

                            <tr>
                                <td>{{ $course->name }}</td>
                                <td>{{ $course->department }}</td>
                                <td>{{ $course->level }}</td>
                                <td>{{ $course->code }}</td>
                            </tr>

                    @endforeach

                    </tbody>
                </table>

            </div>


        </div>
    </div>
@endsection
