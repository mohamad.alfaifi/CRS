@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                    @if (session('status'))
                        <div class="alert alert-success alert-dismissable">
                            <strong>Success!</strong>&nbsp;{{ session('status') }}
                        </div>
                    @endif

                        <form method="post" action="{{ url('/admin/courses/set') }}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="course">CHOOSE A COURSE:</label>
                                <select class="form-control" name="course" id="course">

                                    @foreach($courses as $course)
                                        <option value="{{ $course->id }}">{{ $course->name }} - {{ $course->code }} - level({{ $course->level }})</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="teacher">THE TEACHER:</label>
                                <select class="form-control" name="teacher" id="teacher">

                                    @foreach($teachers as $teacher)
                                        <option value="{{ $teacher->id }}">{{ $teacher->name }} - {{ $teacher->department }}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="day">DAY OF LITERATURE:</label>
                                <select class="form-control" name="day" id="day">
                                    <option value="1">Sunday</option>
                                    <option value="2">Monday</option>
                                    <option value="3">Tuesday</option>
                                    <option value="4">Wednesday</option>
                                    <option value="5">Thursday</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="starts_at">STARTS AT:</label>
                                <select class="form-control" name="starts_at" id="starts_at">
                                    <option>08:00</option>
                                    <option>09:00</option>
                                    <option>10:00</option>
                                    <option>11:00</option>
                                    <option>12:00</option>
                                    <option>13:00</option>
                                    <option>14:00</option>
                                    <option>15:00</option>
                                    <option>16:00</option>
                                    <option>17:00</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="ends_at">ENDS AT:</label>
                                <select class="form-control" name="ends_at" id="ends_at">
                                    <option>08:50</option>
                                    <option>09:50</option>
                                    <option>10:50</option>
                                    <option>11:50</option>
                                    <option>12:50</option>
                                    <option>13:50</option>
                                    <option>14:50</option>
                                    <option>15:50</option>
                                    <option>16:50</option>
                                    <option>17:50</option>
                                </select>
                            </div>


                            <button type="submit" class="btn btn-default">SHOW IT TO STUDENTS!</button>
                        </form>


            </div>
        </div>
    </div>
@endsection
