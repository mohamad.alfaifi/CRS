@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Your Registered Courses:</h1>
            @if (count($timings) == 0)

                <center>
                    <div class="well well-lg">You have not added any course yet! you should totally <a href="{{ url('/student/courses/list') }}">add some courses</a></div>
                </center>

            @else

                <div class="panel panel-default">
                    {{--<div class="panel-heading">Choose courses and add them</div>--}}

                    <div class="panel-body" style="padding: 0;">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif


                        <table class="table table-bordered table-hover" style="margin: 0;">
                            <thead>
                            <tr>
                                <th>NAME</th>
                                <th>TIME</th>
                                <th>DAY</th>
                                <th>TEACHER</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($timings as $timing)

                                <tr>
                                    <td>{{ $timing->course->name }} ({{ $timing->course->code }})</td>
                                    <td>{{ $timing->starts_at }}&nbsp;&nbsp;-&nbsp;&nbsp;{{ $timing->ends_at }}</td>
                                    <td>{{ $timing->day }}</td>
                                    <td>{{ $timing->teacher->name }}</td>
                                    <td><a href="/student/courses/{{ $timing->id }}/delete/" class="text-danger">- REMOVE</a></td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            @endif
        </div>
    </div>
</div>
@endsection
