@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Add your favorite courses:</h1>
            @if (count($timings) == 0)

                <center>
                    <div class="well well-lg">When the administrators of Jazan University add courses they will show up here.</div>
                </center>

            @else
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('collision'))
                    <div class="alert alert-danger">
                        <strong>Collision!</strong> {{ session('collision') }}
                    </div>
                @endif
                @if (session('threshold'))
                    <div class="alert alert-info">
                        <strong>Whoops!</strong> {{ session('threshold') }}
                    </div>
                @endif

                <div class="panel panel-default">                    
                    <div class="panel-body" style="padding: 0;">


                        <table class="table table-hover">
                            <thead class="bg-success">
                            <tr>
                                <th>NAME</th>
                                <th>LEVEL</th>
                                <th>DAY</th>
                                <th>TIME</th>
                                <th>CODE</th>
                                <th>TEACHER</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($timings as $timing)

                                <tr>
                                    <td>{{ $timing->course->name }}</td>
                                    <td>{{ $timing->course->level }}</td>
                                    <td>{{ $timing->day }}</td>
                                    <td>{{ $timing->starts_at }}&nbsp;&nbsp;-&nbsp;&nbsp;{{ $timing->ends_at }}</td>
                                    <td>{{ $timing->course->code }}</td>
                                    <td>{{ $timing->teacher->name }}</td>
                                    <td><a href="/student/courses/{{ $timing->id }}/add/">+ ADD</a></td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            @endif
        </div>
    </div>
</div>
@endsection
